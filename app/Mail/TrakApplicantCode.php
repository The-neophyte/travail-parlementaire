<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Lang;

class TrakApplicantCode extends Mailable
{
    use Queueable, SerializesModels;

    public $title;
    public $user_name;
    public $key;
    public $cin;
    public $created_at;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $user_name, $cin, $key, $created_at)
    {
        $this->title = $title;
        $this->user_name = $user_name;
        $this->cin = $cin;
        $this->key = $key;
        $this->created_at = $created_at;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.connectInformation')
            ->subject(Lang::get('layout.email_subject'))
            ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
