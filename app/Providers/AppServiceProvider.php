<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer(
            'pages.request.inc._start_request',
            function ($view) {
                $districts = \App\Models\District::Active()
                    ->whereHas(
                        'users',
                        function ($query) {
                            $query->where('role_id', env('DEPUTE_ID'));
                        }
                    )
                    ->orWhereHas('profiles')
                    ->get();

                $view->with(compact('districts'));
            }
        );
        view()->composer(
            'pages.request.inc._new_request',
            function ($view) {
                $categories = \App\Models\Category::Active()->get();
                $view->with(compact('categories'));
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
