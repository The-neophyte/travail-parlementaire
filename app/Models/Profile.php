<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'district_id',
        'name_ar',
        'name_fr',
        'name_en',
        'avatar',
        'email',
        'phone_number',
        'active'
    ];

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
