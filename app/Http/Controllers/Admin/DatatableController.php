<?php

namespace App\Http\Controllers\Admin;

use App\Models\Applicant;
use App\Models\GovRequest;
use App\Models\Profile;
use App\Models\Role;
use App\Models\Status;
use App\Models\UserType;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class DatatableController extends Controller
{

    public function admin_users_list()
    {
        $users = \App\User::select(['id', 'name', 'email', 'role_id', 'created_at', 'updated_at'])
            ->with('role');

        return DataTables::of($users)
            ->addColumn(
                'role',
                function ($user) {
                    return $user->role ? $user->role->name : '--';
                }
            )
            ->addColumn(
                'action',
                function ($user) {
                    $removed_link = route('remove_user',[$user->id]);
                    $action = '<a href="#" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a> ';
                    $action .= '<a href="' . route('edit_user_admin',[$user->id]) . '" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a> ';
                    $action .= '<a href="#"
                                class="btn btn-sm btn-danger"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''.$removed_link.'\')
                                }">
                            <i class="fa fa-ban"></i> Remove
                        </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function users_list()
    {
        $users = Applicant::select(['id', 'cin', 'name', 'email', 'created_at']);
        return DataTables::of($users)
            ->addColumn(
                'action',
                function ($user) {
                    $action = '<a href="' . route(
                            'show_applicant',
                            [$user->id]
                        ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a> ';
                    return $action;
                }
            )
            ->make(true);
    }

    public function profiles_list()
    {
        $profiles = Profile::select(
            ['id', 'district_id', 'name_en', 'avatar', 'email', 'phone_number', 'active', 'created_at']
        );
        return DataTables::of($profiles)
            ->editColumn(
                'avatar',
                function ($profile) {
                    return asset($profile->avatar);
                }
            )
            ->addColumn(
                'district',
                function ($profile) {
                    $action = $profile->district->name_en;
                    return $action;
                }
            )
            ->addColumn(
                'action',
                function ($profile) {
                    $removed_link = route(
                        'remove_profile',
                        [$profile->id]
                    );
                    $action = '<a href="' . route(
                            'edit_profile',
                            [$profile->id]
                        ) . '" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>';
                    $action .= '<a href="#"
                                class="btn btn-sm btn-danger"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''.$removed_link.'\')
                                }">
                            <i class="fa fa-ban"></i> Remove
                        </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function category_list()
    {
        $categories = \App\Models\Category::select(['id', 'name_ar', 'name_fr', 'name_en', 'created_at']);

        return DataTables::of($categories)
            ->addColumn(
                'action',
                function ($category) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-danger"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('remove_category',[$category->id]) .'\')
                                }">
                            <i class="fa fa-ban"></i> Remove
                        </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function district_list()
    {
        $districts = \App\Models\District::select(['id', 'name_en', 'name_fr', 'name_ar', 'created_at']);
        return DataTables::of($districts)
            ->addColumn(
                'action',
                function ($district) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-danger"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('remove_district',[$district->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Remove
                                </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function status_list()
    {
        $status = \App\Models\Status::select(['id', 'parent_status', 'name', 'color', 'created_at']);
        return DataTables::of($status)
            ->addColumn(
                'parent',
                function ($statu) {
                    if ($statu->parent_status) {
                        $parent = Status::where('id', $statu->parent_status)->first()->name;
                    } else {
                        $parent = '--';
                    }
                    return $parent;
                }
            )
            ->addColumn(
                'action',
                function ($statu) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-danger"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('remove_status',[$statu->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Remove
                                </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function ministry_list()
    {
        $ministry = \App\Models\Ministry::select(['id', 'name_en', 'active', 'created_at']);
        return DataTables::of($ministry)
            ->addColumn(
                'action',
                function ($ministry) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-danger"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('remove_ministry',[$ministry->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Remove
                                </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function requests_list(Request $request)
    {
        $permission = $permissions = json_decode(auth()->user()->role->permissions);

        if (
            (auth()->user()->role->super_admin == 1) ||
            (isset($permission->all_requests->is_visible))
        ) {
            $requests = \App\Models\Request::with('applicant')
                ->filter($request->all())
                ->with('status')
                ->with('category')
                ->with('district')
                ->with('user');
        } else {
            if (isset($permission->closed_request->is_visible)) {
                if (isset($permission->new_request->is_visible)) {
                    if (isset($permission->selected_request->is_visible)) {
                        $requests = \App\Models\Request::where('user_id', auth()->user()->id)
                            ->orWhere('user_id', null)
                            ->orWhere('closed', 1)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    } else {
                        $requests = \App\Models\Request::where('user_id', null)
                            ->orWhere('closed', 1)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    }
                } else {
                    if (isset($permission->selected_request->is_visible)) {
                        $requests = \App\Models\Request::where('user_id', auth()->user()->id)
                            ->orWhere('closed', 1)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    } else {
                        $requests = \App\Models\Request::where('closed', 1)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    }
                }
            } else {
                if (isset($permission->new_request->is_visible)) {
                    if (isset($permission->selected_request->is_visible)) {
                        $requests = \App\Models\Request::where('user_id', auth()->user()->id)
                            ->orWhere('user_id', null)
                            ->where('closed', 0)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    } else {
                        $requests = \App\Models\Request::where('user_id', null)
                            ->where('closed', 0)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    }
                } else {
                    if (isset($permission->selected_request->is_visible)) {
                        $requests = \App\Models\Request::where('user_id', auth()->user()->id)
                            ->orWhere('closed', 0)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    } else {
                        $requests = \App\Models\Request::where('closed', 0)
                            ->filter($request->all())
                            ->with('applicant')->with('status')->with('category')->with('district')->with('user');
                    }
                }
            }
        }

        return DataTables::of($requests)
            ->addColumn(
                'action',
                function ($request) {
                    $action = '<a href="' . route('show_admin_request', [$request->id]) . '" class="btn btn-sm btn-primary">
                    <i class="fa fa-eye"></i> view</a> ';
                    return $action;
                }
            )
            ->make(true);
    }

    public function requests_list_applicant($applicant_id)
    {
        if (auth()->user()->role->super_admin == 1) {
            $requests = \App\Models\Request::where('applicant_id', $applicant_id)
                ->with('applicant')
                ->with('status')
                ->with('category')
                ->with('district')
                ->with('user');
        } else {
            $requests = \App\Models\Request::where('applicant_id', $applicant_id)
                ->where(
                    function ($query) {
                        $query->where('user_id', auth()->user()->id)->orWhereNull('user_id');
                    }
                )
                ->with('applicant')
                ->with('status')
                ->with('category')
                ->with('district')
                ->with('user');
        }

        return DataTables::of($requests)
            ->addColumn(
                'action',
                function ($request) {
                    $action = '<a href="' . route(
                            'show_admin_request',
                            [$request->id]
                        ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> view</a> ';
                    return $action;
                }
            )
            ->make(true);
    }

    public function users_types_list()
    {
        $roles = Role::select('id', 'name', 'created_at');
        return DataTables::of($roles)
            ->addColumn(
                'action',
                function ($role) {
                    if ($role->id != 1) {
                        $action = '<a href="' . route(
                                'show_user_type',
                                ['role_id' => $role->id]
                            ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>';
                        $action .= '<a href="' . route(
                                'edit_user_type',
                                ['role_id' => $role->id]
                            ) . '" class="btn btn-sm btn-warning" style="margin-left: 5px"><i class="fa fa-edit"></i> Edit</a>';
                        $action .= '<a href="#"
                                class="btn btn-sm btn-danger"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('remove_user_type',['role_id' => $role->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Remove
                                </a>';
                    } else {
                        $action = '<a href="' . route(
                                'show_user_type',
                                ['role_id' => $role->id]
                            ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>';
                    }
                    return $action;
                }
            )
            ->make(true);
    }

    public function gov_requests()
    {
        $govRequest = GovRequest::select('id', 'request_unique_number', 'user_id', 'title', 'category_id', 'ministry_id', 'created_at')
            ->with('user')->with('ministry')->with('category');

        return DataTables::of($govRequest)
            ->addColumn(
                'user',
                function ($Request) {
                    return $Request->user->name;
                }
            )
            ->addColumn(
                'ministry',
                function ($Request) {
                    if ($Request->ministry) {
                        return $Request->ministry->name_en;
                    }
                    return '--';
                }
            )
            ->addColumn(
                'category',
                function ($Request) {
                    if ($Request->category) {
                        return $Request->category->name_en;
                    }
                    return '--';
                }
            )
            ->addColumn(
                'action',
                function ($Request) {
                    $action = '<a href="' . route(
                            'show_gov_request',
                            ['id' => $Request->id]
                        ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>';
                    $action .= '<a style="margin-left:5px;" href="' . route(
                            'edit_gov_request',
                            ['id' => $Request->id]
                        ) . '" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>';
                    return $action;
                }
            )
            ->make(true);
    }

}
