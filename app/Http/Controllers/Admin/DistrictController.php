<?php

namespace App\Http\Controllers\Admin;

use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function index()
    {
        return view('admin.super.settings.district.index');
    }

    public function create()
    {
        return view('admin.super.settings.district.create_district');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name_ar' => 'required',
                'name_fr' => 'required',
                'name_en' => 'required',
                'active' => 'required'
            ]
        );
        District::create(
            [
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'active' => $request->active
            ]
        );
        session()->flash('message', 'A new district has been created!');
        return redirect()->route('districts');
    }

    public function remove($district_id)
    {
        $district = District::where('id', $district_id)->first();
        $district->delete();
        session()->flash('message', 'A district has been removed!');
        return redirect()->route('districts');
    }

}
