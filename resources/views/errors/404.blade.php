@extends('layouts.app_errors')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>@lang('layout.page_404')</h1>
                <h2>@lang('layout.not_found')</h2>
                <ul>
                    <li><a href="{{ url(app()->getLocale()) }}">@lang('layout.go_to_home')</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection
