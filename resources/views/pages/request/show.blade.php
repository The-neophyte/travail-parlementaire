@extends('layouts.app')

@push('style')
    <style>
        .slide{
            background-image: none;
            background-color: #fcfcfc;
            color: #222;
        }
        .title{
            color: #222;
            font-weight: lighter;
            font-size: 30px;
        }
        #footer .container{
            color: #222;
        }
    </style>
@endpush

@section('content')
    <section class="slide">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                <li>
                                    <em>Créé par <b>{{ $user_request->applicant->name }}</b>, le {{ $user_request->created_at }}</em>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <div class="card">
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                <li>
                                    @if($user_status)
                                        <span style="color: {{ '#'.$user_status->color }}"><em>{{ $user_status->name }}</em> <i class="fa fa-circle"></i></span>
                                    @else
                                        <span style="color: #757575"><em>En attente de validation</em> <i class="fa fa-circle"></i></span>
                                    @endif
                                    @if($user_request->updated_at != $user_request->created_at)
                                        <br>Mise a jour le {{ $user_request->updated_at }}
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="border: 2px dashed #ddd; background-color: #fff;">
            <div>
                <div class="row" style="padding-top: 20px; padding-bottom: 20px;">
                    @if($flash = session('message'))
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                                {{ $flash }}
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div style="margin-bottom: 30px;">
                            <h1 class="title" style="text-align: {{ app()->getLocale() == 'ar' ? 'right' : 'left' }}">{{ $user_request->title }}</h1>
                            <div>
                                <em class="btn btn-primary">
                                    @if(app()->getLocale() == 'ar')
                                        {{ $user_request->category->name_ar }}
                                    @elseif(app()->getLocale() == 'fr')
                                        {{ $user_request->category->name_fr }}
                                    @else
                                        {{ $user_request->category->name_en }}
                                    @endif
                                </em>
                                @if($user_request->proof_file)
                                    <div style="margin-top: 15px; margin-bottom: 15px;">
                                        <a href="{{ asset($user_request->proof_file) }}" class="btn btn-primary">Preuve de contact</a>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <hr>
                                        <p style="font-size: 18px;">
                                            {{ $user_request->description }}
                                        </p>
                                    <hr>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-md-7">
                                <comment-component
                                    :app_url="'{{url('/')}}'"
                                    :applicant="{{ $user_request->applicant }}"
                                    :request="{{ $user_request }}" ref="comments"></comment-component>
                            </div>

                            <div class="col-md-5">
                                <file-component
                                    :app_url="'{{url('/')}}'"
                                    :applicant="{{ $user_request->applicant }}"
                                    :request="{{ $user_request }}"></file-component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
