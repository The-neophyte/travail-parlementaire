@extends('layouts.app')

@section('content')
    <section class="container" style="padding-top: 100px; padding-bottom: 100px;">
        <div class="row justify-content-center">
            @if($flash = session('message'))
                <div class="col-md-8">
                    <div class="alert alert-success" style="font-size: 20px;">
                        <i class="fa fa-bell" aria-hidden="true"></i>
                        {{ $flash }}
                    </div>
                </div>
            @endif
        </div>

        <div class="row justify-content-center">
            <div class="col-8">
                @include('pages.request.inc._follow_request')
            </div>
        </div>
    </section>
@endsection
