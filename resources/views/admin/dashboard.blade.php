@extends('admin.layouts.app')

@section('section_title')
    Dashboard
@endsection

@section('content')
    <div class="card shadow-sm">
        <div class="card-header"></div>

        <div class="card-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="card-title">New Requests</h2>
                        </div>
                        <div class="card-body">
                            <span style="font-size: 35px;">{{ $new_request_count }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="card-title">Closed Requests</h2>
                        </div>
                        <div class="card-body">
                            <span style="font-size: 35px;">{{ $closed_request_count }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="card-title">Gov request without answer</h2>
                        </div>
                        <div class="card-body">
                            <span style="font-size: 35px;">{{ $gov_request_without_answer }}</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
