<nav class="col-md-3">
    <div class="card settings-card shadow-sm">
        <ul class="nav flex-column nav-pills" aria-orientation="vertical">
            <a href="{{ url('admin') }}" class="nav-link">
                Dashbord
            </a>
        </ul>
        <div class="card-header">
            <span>Admin</span>
        </div>
        <div class="card-body">
            <ul class="nav flex-column nav-pills">
                <li class="nav-item">
                    <a href="{{ route('requests') }}" class="nav-link {{ (\Request::route()->getName()== 'requests')?'active':'' }} ">
                        Requests
                    </a>
                </li>
                @can('applicant')
                    <li class="nav-item">
                        <a href="{{ route('applicant') }}" class="nav-link {{ (\Request::route()->getName()== 'applicant')?'active':'' }}" >
                            Applicant
                        </a>
                    </li>
                @endcan
                @can('gov_request')
                    <li class="nav-item">
                        <a href="{{ route('gov_request') }}" class="nav-link {{ (\Request::route()->getName()== 'gov_request')?'active':'' }}" >
                            Gov Request
                        </a>
                    </li>
                @endcan
            </ul>

            @can('user_managemen_roles')
                <div class="sub_title">
                    <span>Users Management</span>
                </div>
                <ul class="nav flex-column nav-pills" aria-orientation="vertical">
                    @can('roles')
                        <a href="{{ route('user_types') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['user_types' ,'create_user_type','show_user_type'])?'active':'' }}">
                            Privileges Roles
                        </a>
                    @endcan
                    @can('user_management')
                        <a href="{{ route('admin_users') }}" class="nav-link has-sub-menu {{ in_array(\Request::route()->getName(), ['admin_users' ,'create_admin_user'])?'active':'' }}">
                            Users
                        </a>
                        <a href="{{ route('profiles') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['profiles','create_profile'])?'active':'' }}">
                            Profiles
                        </a>
                    @endcan
                </ul>
            @endcan

            @can('settings')
                <div class="sub_title">
                    <span>Settings</span>
                </div>
                <ul class="nav flex-column nav-pills" aria-orientation="vertical">
                    <a href="{{ route('categories') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['categories','create_category'])?'active':'' }}">
                        Categories
                    </a>
                    <a href="{{ route('ministry') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['ministry','create_ministry'])?'active':'' }}">
                        Ministry
                    </a>
                    <a href="{{ route('districts') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['districts','create_district'])?'active':'' }}">
                        Districts
                    </a>
                    <a href="{{ route('status') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['status','create_status'])?'active':'' }}">
                        Status
                    </a>
                </ul>
            @endcan

            @can('super_admin')
                <div class="sub_title">
                    <span>Super Admin</span>
                </div>
                <ul class="nav flex-column nav-pills" aria-orientation="vertical">
                    <a href="#" class="nav-link" active-class="active">
                        Log Users Access
                    </a>
                    <a href="{{ url('logs') }}" class="nav-link">
                        Logs
                    </a>
                </ul>
            @endcan
        </div>
    </div>
</nav>
