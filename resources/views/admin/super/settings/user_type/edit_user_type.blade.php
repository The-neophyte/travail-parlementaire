@extends('admin.layouts.app')

@section('section_title')
    Edit Role
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('update_user_type', ['user_type' => $role->id]) }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-form-label text-md-right">Name</label>
                    <input type="text" name="name" class="form-control" value="{{ $role->name }}" required autofocus>
                </div>
                <edit-roles-component
                    :s_admin="'{{ $role->super_admin?'1':'0' }}'"
                    :roles="'{{ $role->permissions?$role->permissions:null }}'"
                ></edit-roles-component>
                <div>
                    <button class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>

@endsection
