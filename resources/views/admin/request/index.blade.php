@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Requests
@endsection

@section('content')

    <div class="card-header">
        Filter
    </div>
    <div class="card-body">
        <form method="get" class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <option value="">All</option>
                        <option value="waiting" {{ isset($reqs['status'])?$reqs['status']=='waiting'?'selected':'':'' }}>Waiting</option>
                        @foreach($status as $statu)
                            <option value="{{ $statu->id }}" {{ isset($reqs['status'])?$reqs['status']==$statu->id?'selected':'':'' }}>{{ $statu->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>District</label>
                    <select class="form-control" name="district">
                        <option value="">All</option>
                        @foreach($districts as $disticts)
                            <option value="{{ $disticts->id }}" {{ isset($reqs['district'])?$reqs['district']==$disticts->id?'selected':'':'' }}>{{ $disticts->name_en }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Modérateur</label>
                    <select class="form-control" name="user">
                        <option value="">All</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}" {{ isset($reqs['user'])?$reqs['user']==$user->id?'selected':'':'' }}>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Applicant</label>
                    <select class="form-control" name="applicant">
                        <option value="">All</option>
                        @foreach($applicants as $applicant)
                            <option value="{{ $applicant->id }}" {{ isset($reqs['applicant'])?$reqs['applicant']==$applicant->id?'selected':'':'' }}>{{ $applicant->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Created on</label>
                    <date-select></date-select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Has gov request</label>
                    <select class="form-control" name="hasGovReq">
                        <option value="" {{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']==''?'selected':'':'' }} >All</option>
                        <option value="yes" {{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']=='yes'?'selected':'':'' }} >Yes</option>
                        <option value="no" {{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']=='no'?'selected':'':'' }} >No</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12">
                <button class="btn btn-primary" type="submit">Valide</button>
            </div>
        </form>
    </div>

    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <table id="table" class="table table-striped table-hover table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>KEY</th>
            <th>Title</th>
            <th>Applicant</th>
            <th>Modérateur</th>
            <th>District</th>
            <th>Category</th>
            <th>Status</th>
            <th>Closed</th>
            <th style="width: 150px;">Updated at</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>
@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                order: [0,'desc'],
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('/api/admin/requests_list') }}",
                    data: {
                        status: '{{ isset($reqs['status'])?$reqs['status']:null }}',
                        district: '{{ isset($reqs['district'])?$reqs['district']:null }}',
                        user: '{{ isset($reqs['user'])?$reqs['user']:null }}',
                        closed: '{{ isset($reqs['closed'])?$reqs['closed']:null }}',
                        applicant: '{{ isset($reqs['applicant'])?$reqs['applicant']:null }}',
                        date: '{{ isset($reqs['date'])?$reqs['date']:null }}',
                        hasGovReq: '{{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']:null }}',
                    }
                },
                columns: [
                    { "data": "id" },
                    { "data": "key" },
                    {
                        "data": "title",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "applicant",
                        "render": function(data){
                            return data.name;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "user",
                        "render": function(data){
                            return data?data.name:'--';
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "district",
                        "render": function(data){
                            return data.name_en;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "category",
                        "render": function(data){
                            return data.name_en;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "status",
                        "render": function(data){
                            if(data != null){
                                return '<i class="fa fa-circle" style="font-size:20px; color:#' + data.color + ';"></i>'
                            }else{
                                return '<i class="fa fa-circle" style="font-size:20px; color:#757575;"></i> Waiting'
                            }
                            return data;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "closed",
                        "render": function(data){
                            if(data==true){
                                return true
                            }
                            return false;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "updated_at",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false
                    }
                ]
            });
        });
    </script>

@endsection
