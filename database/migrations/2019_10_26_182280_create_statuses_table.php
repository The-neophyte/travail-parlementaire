<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('color');
            $table->unsignedInteger('parent_status')->nullable();
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        $statuses = ['Inséré', 'Fermé', 'Résolu'];

        foreach ($statuses as $status){
            \Illuminate\Support\Facades\DB::table('statuses')->insert([
                'name' => $status,
                'color' => '#252525',
                'active' => 1,
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
