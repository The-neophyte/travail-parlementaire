<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_fr');
            $table->string('name_ar');
            $table->string('name_en');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });
        
         \Illuminate\Support\Facades\DB::table('categories')->insert(
            [
                'name_fr' => 'Autre',
                'name_ar' => 'قطاع آخر',
                'name_en' => 'Other',
                'active' => 1,
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
